import 'dart:convert';
import '../ui/app.dart';
mixin CommonValidation {
  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return 'Email address missing @';
    }
    else if(!value.contains('.')){
      return 'Email address missing .';
    }
    return null;
  }

  String? validateName(String? value) {
    RegExp regEx = new RegExp(r'^[a-zA-Z0-9&%=]+$');
    if (regEx.hasMatch(value!).toString() == 'false'){
      return 'Name can not contains special characters';
    }
    return null;
  }

  String? validateDob(String? value){
    var newstring =value?.substring(value.length - 4);
    var input = int.parse(newstring!);
    if (input > 2022){
      return 'Invalid Year';
    }
  }
}
